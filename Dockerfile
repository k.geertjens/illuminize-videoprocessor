#FROM openjdk:17-alpine
#
#RUN apk update
#RUN apk add gcc gstreamer
#RUN apk add gst-libav gst-plugins-bad gst-plugins-base gst-plugins-good gst-plugins-ugly
#
#RUN echo $PATH
#ENV PATH="$PATH:/usr/lib/gstreamer-1.0"
#RUN echo $PATH
#
#ARG JAR_FILE=./build/libs/video-processor-0.0.1-SNAPSHOT.jar
#ADD ${JAR_FILE} app.jar
#
#EXPOSE 8081
#ENTRYPOINT ["java", "-jar", "/app.jar"]

FROM restreamio/gstreamer:latest-prod

# Install OpenJDK-17
RUN apt-get update && \
    apt-get install -y openjdk-17-jdk && \
    apt-get install -y openjdk-17-jre && \
    apt-get clean

ARG JAR_FILE=./build/libs/video-processor-0.0.1-SNAPSHOT.jar
ADD ${JAR_FILE} app.jar

EXPOSE 8081
ENTRYPOINT ["java", "-jar", "/app.jar"]