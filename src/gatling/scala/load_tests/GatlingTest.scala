package load_tests

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class GatlingTest extends Simulation {

  val httpProtocol = http
    //.baseUrl("https://illuminize-video-processor.livelydune-9edbaf48.westeurope.azurecontainerapps.io")
    .baseUrl("http://localhost:8081")
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

  val scn = scenario("UploadVideos")
    .pause(3, 6)
    .exec(http("Upload test.mp4")
      .post("/video")
        .headers(Map("Content-Type" -> "multipart/form-data"))
        .formUpload("file", "D:\\git\\illuminize-videoprocessor\\src\\gatling\\scala\\load_tests\\test.mp4"))

  setUp(
    scn.inject(rampUsers(500).during(100))
      .protocols(httpProtocol)
  )
}
