package com.illuminize.videoprocessor.pipelines;

import com.illuminize.videoprocessor.services.CloudinaryService;
import com.illuminize.videoprocessor.services.VideoConversionService;
import lombok.Getter;
import org.freedesktop.gstreamer.Bus;
import org.freedesktop.gstreamer.Element;
import org.freedesktop.gstreamer.Gst;
import org.freedesktop.gstreamer.Pipeline;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class VideoConversionPipeline {

    private CloudinaryService cloudinaryService;
    private Pipeline pipeline;
    private Logger logger;

    public VideoConversionPipeline(CloudinaryService cloudinaryService){
        this.cloudinaryService = cloudinaryService;
        logger = Logger.getLogger("c.illuminize.videoprocessor.videoconversionpipeline");
    }

    public String execute(UUID uuid) throws IOException {
        Path playlistRoot = Files.createDirectories(Paths.get("temp_videos/" + uuid + "/"));
        String fullFilePath = playlistRoot.resolve("originalVideo").toString().replace("\\", "/");

        pipeline = (Pipeline) Gst.parseLaunch("filesrc location=" + fullFilePath + " ! decodebin name=demuxer "
                + "demuxer. ! queue ! "
                    + "videoconvert ! x264enc ! muxer. "
                + "demuxer. ! queue ! "
                    + "audioconvert ! avenc_aac ! muxer. "
                + "mpegtsmux name=muxer ! "
                + "hlssink name=sink "
                + "target-duration=5 max-files=0 playlist-length=0");

        Element sink = pipeline.getElementByName("sink");
        String playlistPath = playlistRoot.resolve("playlist.m3u8").toString().replace("\\", "/");
        String segmentPath = playlistRoot.resolve("segment_%05d.ts").toString().replace("\\", "/");
        sink.set("playlist-location", Paths.get(playlistPath));
        sink.set("location", Paths.get(segmentPath));
        sink.set("playlist-root", "https://res.cloudinary.com/dguxtsd45/raw/upload/v1681929947/illuminize/videos/" + uuid + "/");

        pipeline.getBus().connect((Bus.ERROR) (source, code, message) -> {
            System.out.println(message);
            Gst.quit();
        });
        pipeline.getBus().connect((Bus.EOS) (source) -> Gst.quit());

        logger.log(Level.INFO, "Beginning conversion pipeline: " + uuid + " - video file: " + fullFilePath);
        pipeline.play();

        Gst.main();

        pipeline.stop();
        logger.log(Level.INFO, "Pipeline completed: " + uuid + " - video: " + fullFilePath);

        Files.delete(Paths.get(fullFilePath));

        String playlistUrl = "";
        try{
            File playlistFile = new File(playlistPath);
            String folderName = "illuminize/videos/" + uuid;
            playlistUrl = cloudinaryService.upload(playlistFile, folderName, "playlist", "raw");
            logger.log(Level.INFO, "Starting video upload: " + uuid + " to " + playlistUrl);

            File[] directoryListing = new File(playlistRoot.toString()).listFiles();
            for(File file : directoryListing){
                if(file.getName().contains(".ts")){
                    cloudinaryService.upload(file, folderName, file.getName().replace(".ts", ""), "raw");
                }
                Files.delete(file.getAbsoluteFile().toPath());
            }
            new File(playlistRoot.toString()).delete();
            logger.log(Level.INFO, "Video upload complete: " + uuid + " to " + playlistUrl);
        }
        catch (IOException e){
            logger.log(Level.SEVERE, e.getMessage());
        }

        return playlistUrl;


    }

}
