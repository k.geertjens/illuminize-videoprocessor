package com.illuminize.videoprocessor.pipelines;

import org.freedesktop.gstreamer.Gst;
import org.freedesktop.gstreamer.Pipeline;

import java.util.concurrent.TimeUnit;

public class BasicPipeline {

    private static Pipeline pipeline;

    public static void execute() {
        Gst.init();

        pipeline = (Pipeline) Gst.parseLaunch("videotestsrc ! autovideosink");
        pipeline.play();

        Gst.main();

        System.out.println("Wait over!!!");
        Gst.quit();

    }

}
