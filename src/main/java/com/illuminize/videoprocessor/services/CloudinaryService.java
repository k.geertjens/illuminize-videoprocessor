package com.illuminize.videoprocessor.services;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.Map;

@Service
public class CloudinaryService {

    @Value("${cloudinary-cloud-name}") private String cloudinaryCloudName;
    @Value("${cloudinary-api-key}") private String cloudinaryApiKey;
    @Value("${cloudinary-api-secret}") private String cloudinaryApiSecret;

    private Cloudinary cloudinary;

    @PostConstruct
    public void initializeCloudinary(){
        cloudinary = new Cloudinary(ObjectUtils.asMap(
                "cloud_name", cloudinaryCloudName,
                "api_key", cloudinaryApiKey,
                "api_secret", cloudinaryApiSecret,
                "secure", true
        ));
    }

    public String upload(File file, String folder, String publicId, String resourceType) throws IOException {
//        Map uploadResult = cloudinary.uploader().uploadLarge(file, ObjectUtils.asMap(
//                "folder", folder,
//                "public_id", publicId,
//                "resource_type", resourceType,
//                "secure", true
//        ));
        //return uploadResult.get("secure_url").toString();
        return null;
    }
}
