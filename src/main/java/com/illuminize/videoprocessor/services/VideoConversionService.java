package com.illuminize.videoprocessor.services;

import com.illuminize.videoprocessor.pipelines.BasicPipeline;
import com.illuminize.videoprocessor.pipelines.VideoConversionPipeline;
import com.illuminize.videoprocessor.utils.Utils;
import org.apache.commons.lang3.ArrayUtils;
import org.freedesktop.gstreamer.Gst;
import org.freedesktop.gstreamer.Version;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class VideoConversionService {

    @Autowired
    private CloudinaryService cloudinaryService;

    private Logger logger;

    private static final String QUEUE_NAME = "video-upload";

    public VideoConversionService(){
        logger = Logger.getLogger("c.illuminize.videoprocessor.VideoConversionService");

        logger.log(Level.INFO, "Configuring GStreamer");
        Utils.configurePaths();

        File dir = new File("temp_videos/");
        dir.mkdirs();
        logger.log(Level.INFO, "Temp video directory created at " + dir.getAbsolutePath());

        Gst.init();
    }

    @JmsListener(destination = QUEUE_NAME, containerFactory = "jmsListenerContainerFactory")
    public void receiveMessage(String uuidString) throws IOException {
        UUID uuid = UUID.fromString(uuidString);
        runConversionPipeline(uuid);
    }

    public String runConversionPipeline(UUID uuid){
        VideoConversionPipeline pipeline = new VideoConversionPipeline(cloudinaryService);
        String url = "";
        try{
            url = pipeline.execute(uuid);
        }
        catch (IOException e){
            logger.log(Level.WARNING, "Pipeline could not create temp directory");
        }
        return url;
    }

}
