package com.illuminize.videoprocessor.config;

import com.illuminize.videoprocessor.interceptors.UploadVideoInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class InterceptorConfig extends WebMvcConfigurerAdapter {

    private UploadVideoInterceptor uploadVideoInterceptor = new UploadVideoInterceptor();

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(uploadVideoInterceptor);
    }

}
