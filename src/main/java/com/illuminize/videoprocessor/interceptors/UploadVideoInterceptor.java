package com.illuminize.videoprocessor.interceptors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UploadVideoInterceptor implements HandlerInterceptor {

    Logger logger = Logger.getLogger("c.illuminize.videoprocessor.HandlerInterceptor");

    @Autowired
    private JmsTemplate jmsTemplate;
    private static final String QUEUE_NAME = "video-upload";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        try{
            MultipartHttpServletRequest req = (MultipartHttpServletRequest) request;
            MultipartFile file = req.getFile("file");

            UUID uuid = UUID.randomUUID(); //TODO: get UUID from controller
            saveFile(file, uuid);
        }
        catch (Exception e){
            logger.log(Level.INFO, "Interceptor skipped afterCompletion: " + e.getMessage());
        }
    }

    @Async
    public void saveFile(MultipartFile file, UUID uuid){
        try{
            logger.log(Level.INFO, uuid + "Starting file save");
            Thread.sleep(10000);
            Path playlistRoot = Files.createDirectories(Paths.get("temp_videos/" + uuid + "/"));
            String fullFilePath = playlistRoot.resolve("originalVideo").toString().replace("\\", "/");

            InputStream inputStream = file.getInputStream();
            Files.copy(inputStream, Paths.get(fullFilePath), StandardCopyOption.REPLACE_EXISTING);
            inputStream.close();

            logger.log(Level.INFO, uuid + "File saved");
        }
        catch(IOException | InterruptedException e){
            logger.log(Level.SEVERE, e.getMessage());
        }

//        try{
//            jmsTemplate.convertAndSend(QUEUE_NAME, uuid.toString());
//            logger.log(Level.INFO, uuid + "Video file sent to queue");
//        }
//        catch (JmsException e){
//            logger.log(Level.SEVERE, e.getMessage());;
//        }
    }
}
