package com.illuminize.videoprocessor.controllers;

import com.illuminize.videoprocessor.services.VideoConversionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.jms.*;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping("/video")
public class VideoController {

    @Autowired
    VideoConversionService videoConversionService;

    private static final String QUEUE_NAME = "video-upload";

    Logger logger = Logger.getLogger("c.illuminize.videoprocessor.VideoController");

    @PostMapping()
    public ResponseEntity<String> uploadVideo(@RequestParam("file") MultipartFile file){
        UUID uuid = UUID.randomUUID();
        logger.log(Level.INFO, "Request received: " + uuid);

        logger.log(Level.INFO, "Responding: " + uuid);
        return ResponseEntity.ok().body(uuid.toString());
    }

}
