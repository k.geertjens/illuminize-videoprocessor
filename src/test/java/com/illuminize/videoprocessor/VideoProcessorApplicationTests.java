package com.illuminize.videoprocessor;

import com.illuminize.videoprocessor.services.CloudinaryService;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
class VideoProcessorApplicationTests {

	@MockBean
	private CloudinaryService cloudinaryService;

	@Test
	void dummyTest(){
		assertEquals(1, 1);
	}

}
