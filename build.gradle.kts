plugins {
	java
	id("org.springframework.boot") version "2.7.10"
	id("io.spring.dependency-management") version "1.0.15.RELEASE"

	id("io.gatling.gradle") version "3.9.5"
	id("org.sonarqube") version "4.1.0.3113"
	id("jacoco")
}

group = "com.illuminize"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

configurations {
	compileOnly {
		extendsFrom(configurations.annotationProcessor.get())
	}
}

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("io.springfox:springfox-boot-starter:3.0.0")
	implementation("org.freedesktop.gstreamer:gst1-java-core:1.4.0")

	implementation("com.cloudinary:cloudinary-core:1.33.0")
	implementation("com.cloudinary:cloudinary-http44:1.33.0")

	implementation("net.java.dev.jna:jna:5.12.1")
	implementation("net.java.dev.jna:jna-platform:5.12.1")

	implementation("io.gatling:gatling-core:3.9.5")
	implementation("io.gatling:gatling-http:3.9.5")

	implementation("com.azure.spring:spring-cloud-azure-starter:4.7.0")
	implementation("com.azure:azure-security-keyvault-secrets:4.6.1")
	implementation("com.azure:azure-identity:1.8.0")

	implementation("com.azure:azure-messaging-servicebus:7.14.0")
	implementation("org.springframework.boot:spring-boot-starter-activemq:2.7.12")

	compileOnly("org.projectlombok:lombok")

	annotationProcessor("org.projectlombok:lombok")

	testImplementation("org.springframework.boot:spring-boot-starter-test")
}

tasks.withType<Test> {
	useJUnitPlatform()
	finalizedBy(tasks.jacocoTestReport)
}

tasks.jacocoTestReport {
	dependsOn(tasks.test) // tests are required to run before generating the report
	reports {
		xml.required.set(true)
		csv.required.set(true)
		html.outputLocation.set(layout.buildDirectory.dir("jacocoHtml"))
	}
}

sonarqube {
	properties {
		property("sonar.projectKey", "illuminizevideoapi_videoprocessor")
		property("sonar.organization", "illuminizevideoapi")
		property("sonar.qualitygate.wait", true)
		property("sonar.coverage.jacoco.xmlReportPaths", "**/build/reports/jacoco/test/jacocoTestReport.xml")
	}
}